/*
 */
package eu.devs.bean.exporter;

import eu.devs.bean.exporter.annotation.ExportLabel;
import java.io.File;
import java.io.IOException;
import static java.util.Arrays.asList;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests for {@link ExcelExporter}.
 *
 * @author František Špaček
 */
public class ExcelExporterTest {

    private ExcelExporter<Bean> instance;

    @Before
    public void setUp() throws IOException {
        instance = new ExcelExporter<>(asList(new Bean(1, 1500), new Bean(2, 2)),
                File.createTempFile("test", "xls"));
    }

    @Test
    public void testWrite() {
        instance.write();
    }

    public static class Bean {

        private int a;
        private int b;

        public Bean(int a, int b) {
            this.a = a;
            this.b = b;
        }

        @ExportLabel("column_a")
        public int getA() {
            return a;
        }

        public void setA(int a) {
            this.a = a;
        }

        public int getB() {
            return b;
        }

        public void setB(int b) {
            this.b = b;
        }
    }
}
