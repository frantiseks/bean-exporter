/*
 */
package eu.devs.bean.exporter.util;

import eu.devs.bean.exporter.annotation.ExportLabel;
import eu.devs.bean.exporter.exception.BeanException;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import static java.util.Arrays.asList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility class for inspection of Java beans.
 *
 * @author František Špaček
 */
public final class BeansUtil {

    private BeansUtil() {
    }

    public static Map<String, Object> createBeanMap(final Object obj) {
        return createBeanMap(obj, false);
    }

    public static Map<String, Object> createBeanMapWithAnnotation(
            final Object obj) {
        return createBeanMap(obj, true);
    }

    private static Map<String, Object> createBeanMap(final Object obj,
            boolean useAnnotation) throws BeanException {
        if (obj == null) {
            throw new IllegalArgumentException("obj cannot be null");
        }

        final Map<String, Object> beanMap = new LinkedHashMap<>();
        try {
            final List<PropertyDescriptor> properties = asList(Introspector.getBeanInfo(
                    obj.getClass()).getPropertyDescriptors());

            for (PropertyDescriptor property : properties) {
                final Method readMethod = property.getReadMethod();
                if (readMethod != null && !readMethod.getName().contains("Class")) {
                    String propertyLabel = getPropertyLabel(property,
                            readMethod, useAnnotation);
                    beanMap.put(propertyLabel, readMethod.invoke(obj));
                }
            }
        } catch (IntrospectionException ex) {
            throw new BeanException("Unable to inspect bean", ex);
        } catch (IllegalAccessException ex) {
            throw new BeanException("read method is not accessible", ex);
        } catch (IllegalArgumentException ex) {
            throw new BeanException("Invalid argument provided for method "
                    + "invocation", ex);
        } catch (InvocationTargetException ex) {
            throw new BeanException("Unable to invoke read method", ex);
        }

        return beanMap;
    }

    private static String getPropertyLabel(PropertyDescriptor property,
            final Method method, boolean useAnnotation) {
        String propertyLabel = property.getDisplayName();
        if (useAnnotation) {
            final ExportLabel exportLabel = method.getAnnotation(ExportLabel.class);
            if (exportLabel != null) {
                propertyLabel = exportLabel.value();
            }
        }
        return propertyLabel;
    }
}
