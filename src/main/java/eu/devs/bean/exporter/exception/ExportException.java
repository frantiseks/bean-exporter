/*
 */
package eu.devs.bean.exporter.exception;

/**
 *
 * @author František Špaček
 */
public class ExportException extends RuntimeException {

    public ExportException(String message) {
        super(message);
    }

    public ExportException(String message, Throwable cause) {
        super(message, cause);
    }
}
