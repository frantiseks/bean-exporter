/*
 * 
 */
package eu.devs.bean.exporter;

import eu.devs.bean.exporter.exception.ExportException;

/**
 * Defines common exporter API.
 *
 * @author František Špaček
 */
public interface Exporter {

    /**
     * Writes exported data to some output stream.
     *
     * @throws ExportException if any errors occurs
     */
    void write() throws ExportException;
}
