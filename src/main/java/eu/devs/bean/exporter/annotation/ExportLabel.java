/*
 * 
 */
package eu.devs.bean.exporter.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines custom export label name for bean getters.
 *
 * @author František Špaček
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ExportLabel {

    /**
     * Label value which will be used in exported file.
     *
     * @return label value
     */
    String value();
}
