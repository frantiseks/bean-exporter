/*
 */
package eu.devs.bean.exporter.exception;

/**
 *
 * @author František Špaček
 */
public class BeanException extends RuntimeException {

    public BeanException(String message) {
        super(message);
    }

    public BeanException(String message, Throwable cause) {
        super(message, cause);
    }
}
