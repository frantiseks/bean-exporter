/*
 */
package eu.devs.bean.exporter;

import eu.devs.bean.exporter.exception.BeanException;
import eu.devs.bean.exporter.exception.ExportException;
import eu.devs.bean.exporter.util.AssertUtil;
import eu.devs.bean.exporter.util.BeansUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Implementation of {@link Exporter} to be able export data to XLSX files. Implementation is design
 * to create new instance for each export.
 *
 * @author František Špaček
 * @param <T> type of data to export
 */
public class ExcelExporter<T> implements Exporter {

    private final List<T> data;
    private final File exportFile;

    /**
     * Creates instance from provided arguments.
     *
     * @param data data which should be exported.
     * @param exportFile file which should be used for export
     * @throws IllegalArgumentException if provided argument is {@code null} or invalid
     */
    public ExcelExporter(List<T> data, File exportFile) {
        this.data = AssertUtil.checkNotEmpty(data, "data");
        this.exportFile = AssertUtil.checkNotNull(exportFile, "exportFile");
    }

    @Override
    public void write() throws ExportException {
        final List<Map<String, Object>> dataMap = new ArrayList<>();
        try {
            for (T dataItem : data) {
                dataMap.add(BeansUtil.createBeanMapWithAnnotation(dataItem));
            }
        } catch (BeanException ex) {
            throw new ExportException("Unable to process provided data", ex);
        }

        final Workbook wb = new XSSFWorkbook();
        final Sheet sheet = wb.createSheet();
        final Row headerRow = sheet.createRow(0);

        final Set<String> headerLabels = dataMap.get(0).keySet();
        int cellNumber = 0;
        for (String label : headerLabels) {
            headerRow.createCell(cellNumber++).setCellValue(label);
        }

        int rowNumber = 1;
        for (final Map<String, Object> dataRow : dataMap) {
            final Row row = sheet.createRow(rowNumber++);

            cellNumber = 0;
            for (String key : headerLabels) {
                Object obj = dataRow.get(key);
                final Cell cell = row.createCell(cellNumber++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }

        try (FileOutputStream fos = new FileOutputStream(exportFile)) {
            wb.write(fos);
        } catch (IOException ex) {
            throw new ExportException("Unable to write provided data", ex);
        }

    }
}
